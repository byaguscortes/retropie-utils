#!/usr/bin/env python
import sys
import os
import xml.etree.ElementTree as et
from shutil import copyfile

path = os.getcwd()   
games = []
xmlTags = ["desc", "image", "thumbnail", "releasedate", "developer", "publisher", "genre", "players", "favorite", "kidgame", "hidden", "playcount", "lastplayed"]
gamelist = "gamelist.xml"

# open XML file with files details
gameTree = et.ElementTree(file=gamelist)

# check every game in current directory
for file in next(os.walk(path))[2]:
	gameFound = False
	for elem in gameTree.iterfind("game/path"):
		if file in elem.text:
			gameFound = True
			break
	if gameFound is False:
		games.append(file)

# Add new elements 
xmlRoot = gameTree.getroot()
for game in games:
	xmlGame = et.Element("game")
	xmlPath = et.SubElement(xmlGame, "path")
	xmlPath.text = ".\\" + game
	xmlName = et.SubElement(xmlGame, "name")
	xmlName.text = "The File " + game + " should be properly named"	
	for tag in xmlTags:
        	xmlTag = et.SubElement(xmlGame, tag)
	xmlRoot.append(xmlGame)

# Save modified file
copyfile(gamelist, gamelist + ".bak")
treeUpdated = et.ElementTree(xmlRoot)
with open(gamelist, "w") as f:
	treeUpdated.write(f)
